﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.LoopExamples
{
    public class DoWhileLoopExamples : MonoBehaviour
    {
        private int counter = 0;
        public int maxCount;

        private void Start()
        {
            SimpleDoWhileLoop(); 
        }

        //Do-While loop wird immer mind. 1 Mal ausgeführt; egal ob alles false ist - While-Loop check Bedingungen ganz am Anfang
        private void SimpleDoWhileLoop()
        {
            do
            {
                Debug.Log("Do-While-Loop BEFORE counting up:" + counter);

                counter++;

                Debug.Log("Do-While loop AFTER counting up: " + counter);
            }
            while (counter < maxCount);
        }
    }
}