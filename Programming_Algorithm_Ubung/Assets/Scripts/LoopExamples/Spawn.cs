﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.LoopExamples
{
    public class Spawn : MonoBehaviour
    {
        public GameObject prefab;
        public int counter;

        private void Start()
        {
            //SpawnGameObject();
            //SpawnPrefab();

            //Set how many to spawn YOURSELF with Unity Editor :D
            for (int i = 0; i < counter; i++)
            {
                SpawnPrefab();
            }
        }

        private void SpawnGameObject() //how to create new GameObject
        {
            GameObject go = new GameObject();
        }

        //Prefab bedeutet: Pre-fabricated -> "Template" which GameObject into Asset -> Edit OG edit all the clones
        private void SpawnPrefab()
        {
            Instantiate(prefab); //Instantiate muss Argument haben
        }
    }
}