﻿using UnityEngine;
using System.Collections;

namespace Assets.LoopExamples
{
    public class ForLoopExamples : MonoBehaviour
    {
        public int maxCount;
        public int breaker;
        public int skip;

        private void Start()
        {
            //SimpleForLoop();
            //ReverseLoop();
            //Loopy();
            //NestedLoop();
            //BreakLoop();
            ContinueLoop();
        }
        private void SimpleForLoop() //Going up until max
        {
            Debug.Log("Starting Lopp...");

            //i kann auch anders sein -> standard: i
            for (int i = 0     ; i < maxCount    ; i++)
              //(starting von 0; go until i = x; i always +1 until end)
            {
                Debug.Log("Loop counting, currently at: " + i); //at whelche i wir sind
            }

            Debug.Log("Loop Stop");
        }

        private void ReverseLoop() //Going down until min
        {
            for (int i = maxCount; i > 0; i--) //wenn 0 zählen >= oder >-1
            {
                Debug.Log ("Counting in reverse, currently at:" +i);
            }
        }

        private void Loopy() //keine End-Bedingung oder End-Bedingung kann nicht false: Infinite loop :((
        {
            for (; ;)
            {
                Debug.Log("Loop counting");
            }
        }

        private void NestedLoop() //In Loop = tick -> Outer loop = tick
        {
            for (int i = 0; i < maxCount; i++)
            {
                for (int j = 0; j < maxCount; j++) //Zuerst wird inner ausgeführt, dann äußere
                {
                    Debug.Log("Inner Loop, currently at: " + j);
                }
                Debug.Log("Outer Loop, currently at: " + i);
            }
        }

        private void BreakLoop() //break loop before it reaches max number
        {
            for (int i = 0; i < maxCount; i++)
            {
                if (i == breaker)
                {
                    Debug.Log("We stop right here at: " + breaker);
                    break; //-> Dead stop! Won't continue whatever below
                }
                Debug.Log("Counting loop. Currently at: " + i);
            }
            Debug.Log("End of loop :)");
        }

        private void ContinueLoop()
        {
            for (int i = 0; i < maxCount; i++)
            {
                if (i == skip)
                {
                    Debug.Log("Skipping this value: " + skip);
                    continue; //continue with the thing below
                }
                Debug.Log("Counting loop. Currently at: " + i);
            }
        }
    }
}