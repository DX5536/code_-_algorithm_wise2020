﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.LoopExamples
{
    public class WhileLoopExamples : MonoBehaviour //while-loop: Alles Manuel machen vs For-Loop -> Mehr Kontrolle am codes
    {
        private int counter = 0; //MUSS von 0 starten -> private / hidden machen um anderen nicht zu editieren
        public int breaker;
        public int maxCount;

        private void Start()
        {
            //SimpleWhileLoop();
            BreakWhileLoop();
        }

        private void SimpleWhileLoop()
        {
            while (counter < maxCount) //während counter als maxCount, das ... passiert
            {
                Debug.Log("While loop counting: " + counter);
                GameObject go = new GameObject();

                counter++; //sonst ist counter IMMER kleiner as maxCount -> Infinite loop :(
            }
        }

        private void BreakWhileLoop()
        {
            while (counter < maxCount)
            {
                Debug.Log("While loop counting: " + counter);
                counter++;

                if (counter == breaker)
                {
                    Debug.Log("Getting out early!");
                    GameObject go = new GameObject(); //nur 1 GameObject wird gespawnt
                    break;
                }
            }
        }
    }
}