﻿namespace ClassExamples  //"virtuelle übergeordnete Ordner". Namespace ist kein Datentyp
{
    public class SimpleClassWithVariables
    {
        private int intExampleWithoutValue; //automatically 0
        private float floatExampleWithoutValue; //automatically 0

        private int intExampleWithValue = 5;
        private float floatExampleWithValue = 0.5f;

        private bool boolExampleWithoutValue; //automatically false
        private bool boolExampleWithValue      = true;
        private bool otherboolExampleWithValue = false;

        private string stringExampleWithoutValue;
        private string stringExampleWithValue = "Hallo";
    }
}