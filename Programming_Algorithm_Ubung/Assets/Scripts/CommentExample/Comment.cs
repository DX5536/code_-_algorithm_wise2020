﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.CommentExample
{
    public class Comment : MonoBehaviour
    {
        //Das ist die ID für eine Entity
        private int entityID;

        //Wird später benutzt für eine Abfrage ob die Entity
        //am Leben ist
        private bool isEntityAlive;

        /*
         * Das ist der Name der Entity
         * Sollte im Unity Inspector gesetzt werden
         * Bei Gelegenheit dann auch exposen :3c
         */
        private string entityName;

        private void Start()
        {
            LogEntityId(); //Hover to see Summary contents
        }

        /// <summary>
        /// Printet die ID einer Entity in das Console Window :D 
        /// </summary>
        public void LogEntityId()
        {
            ///Mach nicht so viel Sinn here. Mehr Sinn über die Methode (NICHT IN) wie oben
            Debug.Log(entityID); //Invisible to PC thus unable to read
        }

    }
}

