﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InterfaceExamples
{
    public class DamageManager : MonoBehaviour
    {

        public IDamageAble[] damageAble; //Klasse: IDamageAble[] : Beziehen von alle Scripts, die IDamageAble haben/benützen

        private void Start()
        {
            for (int i = 0; i < damageAble.Length; i++)
            {
                damageAble[i].Damage(100);
            }
        }
    }
}