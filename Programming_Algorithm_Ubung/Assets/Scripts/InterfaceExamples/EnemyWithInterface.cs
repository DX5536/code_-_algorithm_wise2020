﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.InterfaceExamples
{
    public class EnemyWithInterface : MonoBehaviour, ILog, IEntity, IDamageAble
    {
        //ILog
        public void Log(object message) //Muss direkt 1:1 wie ILog
        {
            Debug.Log(message); //Das ist Logik. Interface hat nur Aufbau eine Methode
        }

        //IEntity
        [SerializeField]
        private string entityName;
        public string EntityName
        {
            get { return entityName; }
            set { entityName = value; }
        }

        //IDamageAble
        [SerializeField]
        private int health;
        public void Damage(int amount)
        {
            health -= amount / 2;
        }

        public void Damage(string entityName, int amount)
        {
            health -= amount / 2;
            Log(EntityName + " is hurt by: " + amount);
        }
    }
}