﻿using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.InterfaceExamples
{
    public interface IDamageAble
    {
        void Damage(int amount);
        void Damage(string entityName, int amount);
    }
}