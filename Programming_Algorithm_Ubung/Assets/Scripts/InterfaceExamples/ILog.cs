﻿using UnityEditor;
using UnityEngine;

namespace Assets.Scripts.InterfaceExamples
{
    //Interface ist alle public
    //In Interface kann man KEINE Variable einlegen -> Nur Properties (get set)
    public interface ILog //Microsoft Standard -> Beginnint mit I
    {
        //Interface hat KEIN Logik
        void Log(object message);

        //Klasse kann nur EINE Klasse erben
        //Klasse kann unlimited Interface erben
    }
}