﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Help
{
    public class Dragon: MonoBehaviour
    {
        public int health = 100;

        //Methose nur in Methode aufrufen
        private void Start()
        {
            Damage();
        }

        public void Damage (int DamageAmount = 0) /*Paramether here. Parameter gibt us flexibilität*/
        {
            health -= DamageAmount;
        }
    }
}