﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.Help
{
    public class DragonSlayer: MonoBehaviour
    {
        public int attackPower; //0

        public Dragon dragon; //var von type Dragon.cs, referieren um .-Operator zu benützen kann

        private void Start()
        {
            dragon.Damage(attackPower); //Damage to dragon will be based on attackPower now, instead of being rigid to 1 number

            //.-Operation: Zugriff auf eine Klasse einer Methode andere script
        }
    }
}