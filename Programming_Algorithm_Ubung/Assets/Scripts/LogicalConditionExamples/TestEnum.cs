﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.LogicalConditionExamples
{
    public enum TestEnum //stat "public class TestEnum" -> "public enum TestEnum"
    {
        NONE = 0, //enum all in Capital or First Captital
        IDLE = 1, //es kommt KOMMA , hin   
    }

    public enum PlayerState //Enum is gut für State
    {
        None = 0,
        Idle = 1,
        Walk = 2,
        Run = 3 //Komma am letzte Zeile kann weglassen -> Lieber nicht *smh*
    }

    public enum Spell
    {
        None = 0,
        Fireball = 1,
        Blizzard = 2,
    }
}