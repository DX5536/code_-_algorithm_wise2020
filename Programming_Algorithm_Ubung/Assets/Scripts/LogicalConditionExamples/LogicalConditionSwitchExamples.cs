﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.LogicalConditionExamples
{
    public class LogicalConditionSwitchExamples: MonoBehaviour
    {
        public int playerHealth = 100;
        public string playerName = "xXxNoScopeKillerxXx";

        private void Update()
        {
            HealthSwitch();
            NameSwitch();
        }

        private void HealthSwitch()
        {
            switch (playerHealth) //Switch-method für health is blödsinn! Zu viele Numbber und kann nicht alles abchecken
            {
                case 10:
                    Debug.Log("Player Health is 10");
                    break;
                case 33:
                    Debug.Log("Player Health is 33");
                    break;
                case 100:
                    Debug.Log("Player Health is Max!");
                    break;

                default: //etwa einbisschen wie "else", Standard und soll immer ausgeführt -> wenn keine "case" eg. special cases, stimmt
                    Debug.Log("Health!");
                    break;
            }

            Debug.Log("Method end reached");
        }

        private void NameSwitch()
        {
            switch (playerName)
            {
                case "xXxNoScopeKillerxXx":
                    Debug.Log("Yeah, it is a legit player!");
                    break;

                case "xXxNOSCOPEKILLERxXx":
                    Debug.Log("Yeah, it is a legit player!");
                    break;

                case "Slawa":
                    Debug.Log("Dunno about this one...");
                    break;
            }
        }

    }
}