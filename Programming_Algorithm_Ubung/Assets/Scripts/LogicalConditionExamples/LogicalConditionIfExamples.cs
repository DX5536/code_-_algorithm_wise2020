﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.LogicalConditionExamples
{
    public class LogicalConditionIfExamples : MonoBehaviour
    {

        public int currentHealth = 100;
        public int maxHealth = 100;

        public bool playerAlive;
        public bool playerGrounded;
        public bool playerCanJump;

        private void Update()
        {
            //CheckIfHealth();
            //CheckIfElseHealth();

            //CheckJump();
            CheckSomeJump();

            //CheckHealthByElseIf();
        }

        private void NestedHealthCheck()
        {
            if (currentHealth > 0)
            {
                Debug.Log("CurrentHealth is greater than 0");

                if (playerGrounded)
                {
                    playerCanJump = true;

                    if (playerCanJump)
                    {
                        Debug.Log("I can Jump!");
                    }
                }
            }
        }

        //If-Statement
        public void CheckIfHealth()
        {
            if (currentHealth == maxHealth) //currentHealth = maxHealth ->> Zuweisung und ist KEIN Boolean Expression, deshalb error
                                            //currentHealth == maxHealth ->> Kontrolle und ist ein Boolean Expression, deshalb work
            {
                //new code from if-statement
                Debug.Log("Current Health is at max Health, cool!");
            }

            if (currentHealth <= 0) //When HP = 0
            {
                return; //CPS wird die untere if-Statements gar nicht checken
            }

            if (currentHealth < maxHealth) //Kleiner
            {
                Debug.Log("Current Health is smaller!");
            }

                if (currentHealth <= maxHealth) //Kleiner gleich
                {
                    Debug.Log("Current Health is equal or less!");
                }

            if (currentHealth > maxHealth) //Größer
            {
                Debug.Log("Current Health is greater!");
            }

                if (currentHealth >= maxHealth) //Größer gleich
                {
                    Debug.Log("Current Health is greater or equal!");
                }

        }

        //If-Else_Statement
        public void CheckIfElseHealth()
        {
            if (currentHealth == maxHealth) //currentHealth = maxHealth ->> Zuweisung und ist KEIN Boolean Expression, deshalb error
                                            //currentHealth == maxHealth ->> Kontrolle und ist ein Boolean Expression, deshalb work
            {
                //new code from if-statement
                Debug.Log("Current Health is at max Health, cool!");
            }
            else //Else funktioniert NUR mit if -> kann nicht allein existieren!
            {
                Debug.Log("Current Health is not at max Health, not cool!");
            }

            Debug.Log("Will this works? Yes"); //code außerhalb If-Else, execute immer

        }

        private void CheckJump()
        {
            /* if (currentHealth > 0)
            {
                playerAlive = true;
            }

            else
            {
                playerAlive = false;
            }

            if (playerAlive == false) // the same as " if (!playerAlive) "
            {
                Debug.Log("Player is Dead!");
                return;
            }

                if (!playerAlive) // ! = negate condition eg. NOT playerAlive
                {
                    Debug.Log("Player is Dead!");
                    return;
                }

            if (playerAlive == true) // the same as "if (playerAlive)" -> Default: playerAlive = true; Bool Variable is automatisch Boolean Expression
            {
                Debug.Log("Player is Alive!"); 
            } */

            if (playerAlive && playerGrounded) // && bedeutet UND ; Check BEIDE conditions und BEIDE muss true sein
                                                   //Wenn ein condition NICHT true => code skippen
            {
                playerCanJump = true;
                Debug.Log("Player is Alive and can jump!");
            }

            else
            {
                playerCanJump = false;
            }
        }

        private void CheckSomeJump()
        {
            if ((playerAlive || playerGrounded) && currentHealth ==100) //Wie Mathe: playerAlive ODER playerGrounded muss TRUE ; UND dann currentHealth == 100
            {
                playerCanJump = true;
            }

            if (playerAlive || playerGrounded) //Einst davon = true -> If-statement stimmt und wird ausgeführt
            {
                playerCanJump = true;
            }

            else
            {
                playerCanJump = false;
            }
        }

        private void CheckHealthByElseIf()
        {
            if (currentHealth == maxHealth) 
            {
                Debug.Log("CurrentHealth is equal to Maximum Health");
            }

            else if (currentHealth > maxHealth) //spezial Falls. (Theoretisch) 'Unendlich' viel "else if" machen
            {
                Debug.Log("Wait what?! This is not possible since currentHealth can't be greater than max Health! This is a bug.");
            }

            else //wenn "else if" nicht passiert dann "else"
            {
                Debug.Log("CurrentHealth is not equal to maxHealth and is not greater than maxHealth. Probably lesser.");
            }
        }

    }
}