﻿using UnityEngine;
using UnityEditor;

namespace Assets.Scripts.LogicalConditionExamples
{
    public class EnumSwitchCaseExamples : MonoBehaviour
    {
        public Spell spell;

        private void Start()
        {
            //int randomSpellIndex = Random.Range(0, 3); //random Spell
            //spell = (Spell)randomSpellIndex;

            spell = Spell.Fireball;
            //spell = (Spell) 1; -> Gleiche wie oben JEDOCH sehr unübersichtlich!
        }

        private void Update()
        {
            switch (spell)
            {
                case Spell.None:
                    Debug.Log("No Magic Attack Set - This is an ERROR");
                    break;

                case Spell.Fireball:
                    Debug.Log("Player casts Fireball");
                    break;

                case Spell.Blizzard:
                    Debug.Log("Player casts Blizzard");
                    break;

            }
        }
    }
}