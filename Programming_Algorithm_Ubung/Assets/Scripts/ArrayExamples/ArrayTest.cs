﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.ArrayExamples
{
    public class ArrayTest : MonoBehaviour
    {
        //Array: Vielle Variablen unter *das gleiche* DatenType zu packen!

        //Unit Array aber leer -> KEINE Größe + KEINE Werte
        private string[] playerNames; //Array (Liste) -> DatenType[]; Array mit PLURAL zunennen

        //Init Array Element im Code -> Bestimmte Größe + KEINE Werte
        private string[] enemyNames = new string[5];

        //Another way to Init Array. Bestimmte Größe + BESTIMMTE Werte :D
        private string[] weaponNames = new string[5]
        {
            "Axe",
            "Sword",
            "Bow",
            "Staff",
            "Magnum",
        };

        //Unity-Inspector zu maniapulieren
        public int whichWeaponIndex;
        public string newWeaponName;

        private void Start()
        {
            //SetPlayerNames();

            //SetEnemyName();
            //SetEnemyNamesByLoop();

            //ShowWeaponNames();
            //ChangeSingleWeaponName(whichWeaponIndex, newWeaponName);
            ChangeAllWeaponName();
        }


        private void SetPlayerNames()
        {
            //playerNames kopiert alles von enemyNames
            /* playerNames = enemyNames; //-> playerNames hat jetzt 5 Elemente (wie enemyNames)*/

            playerNames = new string[5]
            {
                "Karla",
                "Paul",
                "Evan",
                "Woody",
                "Buzz",
            };

            for (int i = 0; i < playerNames.Length; i++)
            {
                Debug.Log("Player at index: " + i + "/ Player has this name: " + playerNames[i]);
            }
        }

        private void SetEnemyName()
        {
            string enemyName1 = "Blob";
            enemyNames[0] = "Blob"; //Element 1 von 5. 1st Element = Blob

            enemyNames[1] = "Soldier"; //Element 2 von 5. 2nd Element = Soldier
            enemyNames[2] = "Dragon"; //Element 3 von 5. 3rd Element = Dragon
            enemyNames[3] = "Queen"; //Element 4 von 5. 4th Element = Queen
            enemyNames[4] = "King"; //Element 5 von 5. 5th Element = King
            /*enemyNames[5] = "Wolf"; //->Array out of range, 6 Elemente aber Array erlaubt nur 5 */

            //Print only 1 Array
            //Debug.Log(enemyNames[0]); //= Debug.Log(enemyName1);

            //Print all the Array through ForLoop
            for (int i = 0; i < enemyNames.Length ; i++) //i < 5 = i < enemyNames.Length -> Besser ungenaue Value eingeben als bestimmte Zahl + Nicht vertippen!!
            {
                //Debug.Log(enemyNames[enemyNames.Length]); //Length ist nicht anders als ein Zahl aka. 5 (here)
                Debug.Log("Enemy at index: " + i + "/ Enemy has this name: " + enemyNames[i]); // -> i: Auf index wert verlassen! i erhört selber!
            }
        }

        private void SetEnemyNamesByLoop()
        {
            for (int i = 0; i < enemyNames.Length; i++)
            {
                enemyNames[i] = "Blob"; //Alle Element auf Index wird "Blob" heißen
                Debug.Log("Enemy at index: " + i + "/ Enemy has this name: " + enemyNames[i]);
            }
        }

        private void ShowWeaponNames()
        {
            for (int i = 0; i < weaponNames.Length; i++)
            {
                Debug.Log("Weapon at index: " + i + "/ Weapon has this type: " + weaponNames[i]);
            }
        }

        private void ChangeSingleWeaponName(int index, string newName) //wir brauchen ein flexible int-Wert -> Inspector zu manipulieren
        {
            Debug.Log("Change weapon name... ");
            //new weapon name override previous name
            weaponNames[index] = newName;
            ShowWeaponNames();
        }

        private void ChangeAllWeaponName()
        {
            string[] newWeaponNames = new string[5]
            {
                "Scissors",
                "Scalpel",
                "Handgun",
                "Sniper Rilfe",
                "Spear",
            };
            
            for (int i = 0; i < weaponNames.Length; i++)
            {
                //Axe = Scissors
                //weaponNames[0] = Scissors
                weaponNames[i] = newWeaponNames[i];
                //weaponNames wird *override* von newWeaponNames
            }

            ShowWeaponNames();

        }
    }
}