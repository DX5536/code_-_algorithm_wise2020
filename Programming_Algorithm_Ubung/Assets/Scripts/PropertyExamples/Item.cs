﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.PropertyExample
{
    public class Item : MonoBehaviour
    {
        private int weight;

        public int Weight //sieht fasst wie Methode aus aber KEINE (xxx)

        {
            get { return weight; } //same as below {return weight}
            set { weight = value; } // XX-Wert here bleibt XX-Value -> Wert is stone fest   -> ItemManager kann nicht ändern
                                    //Wenn aber value schreiben     -> Wert is "in the end" -> Unity ändert den Wert in Itemmanager
        }
    }
}

