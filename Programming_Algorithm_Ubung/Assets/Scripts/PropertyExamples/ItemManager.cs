﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.PropertyExample
{
    public class ItemManager : MonoBehaviour
    {
        public Item item;

        private void Start()
        {
            SetItemWeight();
            Debug.Log("Current Item Weight:" + item.Weight);
        }

        private void SetItemWeight()
        {
            item.Weight = 5; //Weight ist property -> Property kann private Klasse zugreifen (Klasse kann private bleiben)
        }
    }
}