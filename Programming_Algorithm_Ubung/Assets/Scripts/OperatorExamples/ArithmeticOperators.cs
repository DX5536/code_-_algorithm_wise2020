﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.OperatorExamples
{
    public class ArithmeticOperators : MonoBehaviour
    {
        public int health;
        public int healingAmount;
        public int damageAmount;

        public void AddHealthVariantOne()
        {
            // "+" addieren Zahlen; muss noch old "health" schreiben
            health = health + healingAmount;
        }
                public void AddHealthVariantTwo()
                {
                // "+=" addieren Zahlen; kürzer als oben
                health += healingAmount;
                }

        public void SubtractHealthVariantOne()
        {
            // "-" minus Zahlen; muss noch old "health" schreiben
            health = health - damageAmount;
        }
                public void SubtractHealthVariantTwo()
                {
                // "-=" minus Zahlen; kürzer als oben
                health -= damageAmount;
                }

        public void MultiplyHealthVariantOne()
        {
            // "*" multiplizieren Zahlen; muss noch old "health" schreiben
            health = health * healingAmount;
        }
                public void MultiplyHealthVariantTwo()
                {
                // "*=" addieren Zahlen; kürzer als oben
                health *= healingAmount;
                }

        public void DevideHealthVariantOne() //technically, Devision ist langsamer als andere Operator -> Multiplizieren Kehrwert (Notfalls)
        {
            // "/" dividieren Zahlen; muss noch old "health" schreiben
            health = health / damageAmount;
        }
                public void DevideHealthVariantTwo()
                {
                // "/=" dividieren Zahlen; kürzer als oben
                health /= damageAmount;
                }

        public void Modulo()
        {
            //Modulo gibt den Rest zurück -> 5 / 2 = 2.5 ; 5 Modulo 2 = 1 (0.5 + 0.5) oder 5 Modulo 5 = 0 (5/5=1 kein Rest)
            health = health % damageAmount;

            health %= damageAmount;
        }

        public void IncrementByOne()
        {
            health++; //immer +1 addieren; 100+1 = 101 -> 101+1 = 102 -> ...
        }

        public void DecrementByOne()
        {
            health--; //immer -1 addieren; 100-1 = 99 -> 99-1 = 98 -> ...
        }

    }
}