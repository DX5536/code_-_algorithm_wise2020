﻿using System.Collections;
using UnityEngine;

namespace Assets.Scripts.AttributeExamples
{
    public class AtributeExamples : MonoBehaviour
    {
        //Attribute: so ein tag von C# oder Unity
        //Attribute ist nicht anders als Klasse -> Aber Special form

        //Range ist ein Unity Attribute: [Range(float min_Wert , float max_Wert)]
        //Unity Inspector: Slider, kann nicht mehr oder weniger als Range
        [Range(0f , 100f)]
        public float health;

        //private Variable -> Property!!!! (Sonst Punktabzug)

        //Force Unity to show private Variable in Inspector
        [SerializeField]
        private float standardHealth;
    }
}