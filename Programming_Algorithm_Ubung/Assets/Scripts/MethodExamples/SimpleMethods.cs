﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

namespace Assets.Scripts
{
    public class SimpleMethods : MonoBehaviour
    {
        public int health;
        public string name;

        public void InitHealth() // kommt nicht zurück. "Mindless execution" -> Wir geben Einkaufsliste zu A und A geht einkaufen.
        {
            health = 100;
            name = "Hai Lam";
        }

        public int GetHealth()
        {
            return health; //etwas soll zurück führen. Wir wollen etwas zurück -> Wir geben Einkaufsliste zu A und A gibt spezifisch Sachen zurück (Rest egal).
        }

        public int GetDoubleHealth()
        {
            health *= 2; // -> 100 (health) *2 = 200
            return health; //return ist NICHT zurücksetzen. return BEENDEN auch eine Methode
        }
    }
}