﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class Player : MonoBehaviour
    {
        private int health;
        private int maxHealth; // Argumente name lieber NICHT gleich wie Klasse name
        private int newHealth;

        private void Awake() //
        {
            InitHealth();
            ChangeHealth(newHealth);
        }

        public void InitHealth()
        {
            health = maxHealth;
        }

        public void ChangeHealth (int healthValue) //das gleiche wie "maxHealth" (oben). Naming anders damit kein Konfus gibt -> avoid confusion with same name.
        {
            health = healthValue;
        }

        public int GetHealth()
        {
            return health;
        }

    }
}