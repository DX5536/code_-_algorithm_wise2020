﻿using UnityEngine;
using System.Collections;

namespace MethodExamples  //"virtuelle übergeordnete Ordner"
{
    public class PlayerSideThing : MonoBehaviour
    {
        public int health; //default = 0 but we edit in Unity

        private void Awake() // Awake() starts at the begin of the game. Method very specific to MonoBehaviour
        {
            health = 100; 
        }

        private void Start() //Start() starts after Awake()
        {

        }

        private void Update() //Update() starts after Start(). Logic in here -> will be refresh every sec. Tips: Multiply instead of devide => *0.5 instead of /2
        {
         
        }
    }
}
