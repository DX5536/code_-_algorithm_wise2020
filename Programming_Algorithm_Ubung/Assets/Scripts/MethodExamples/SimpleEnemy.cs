﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts.MethodExamples
{
    public class SimpleEnemy : MonoBehaviour
    {

        public string enemyName;
        public int    enemyHealth;
        public bool   isEnemyAlive;

        private void Awake()
        {
            InitEnemy(50, true); //wenn (50) 1. InitEnemy benützt ; wenn (50, true) 2. InitEnemy benützt
        }

        private void Update() //immer 1 update log per second; Update is Unity specific
        {
            Debug.Log("The name of the enemy:" + enemyName); //zeight in Console damit wir print kann. Debug kann richtig schwer sein -> wenig benützen
            Debug.Log("Current enemy health:"  + enemyHealth);
            Debug.Log("Is Enemy Alive:"        + isEnemyAlive);
        }

        private void InitEnemy(int health)
        {
            enemyHealth = health;
        }

        private void InitEnemy(int health, bool isAlive)
        {
            enemyHealth  = health;
            isEnemyAlive = isAlive;
        }
    }
}