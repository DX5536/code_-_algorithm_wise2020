﻿using UnityEngine;
using System.Collections;

namespace Assets.Scripts
{
    public class PlayerManager : MonoBehaviour //see Unity: Change health Value of playerTwo and PlayerManager's health will be updated
    {
        public Player player;
        public int currentPlayerHealth;

        public void Update()
        {
            CheckForPlayerHealth();
        }

        public void CheckForPlayerHealth()
        {
            currentPlayerHealth = player.GetHealth(); //. signals that we want access to player (from PlayerTwo). GetHealth() same data type
        }

        public void HealPlayer()
        {
            player.ChangeHealth(600);
        }
    }
}